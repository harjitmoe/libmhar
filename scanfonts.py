#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-
# By HarJIT in 2021, ©.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys, os, glob, asyncio, collections
import unicodedata as ucd
from fontTools.ttLib import TTFont, TTCollection
from libmhar import G, C

asyncio.run(
    G(["/usr/share/fonts/**/*.", "/media/*/*/Windows/Fonts/**/*."]).bind1(lambda prefix:
        G(["ttf", "otf", "ttc", "TTF", "OTF", "TTC"]).map1(lambda suffix: prefix + suffix)
    ).map1(lambda glob_string: 
        C.delay(glob.glob, glob_string, recursive=True).bind1(lambda glob_results:
            G(glob_results).map1(lambda font_file_path:
                (#conditional
                    C.delay(TTFont, font_file_path).map1(lambda font: G([font]))
                if not font_file_path.endswith(".ttc") else
                    C.delay(TTCollection, font_file_path).map1(lambda ttc: G(ttc.fonts))
                ).bind1(lambda fonts_in_file:
                    fonts_in_file.bind1(lambda font:
                        G(collections.OrderedDict(
                            G(font["cmap"].tables).filter1(lambda table:
                                table.isUnicode()
                            ).filter1(lambda table:
                                not any(G(sys.argv[1:]).filter1(lambda arg:
                                    len(arg) >= 2 and arg.startswith("-")
                                ).map1(lambda i:
                                    ord(i[1:].rstrip("\uFE0E\uFE0F")) in table.cmap
                                ))
                            ).bind1(lambda table: 
                                G(sys.argv[1:]).filter1(lambda arg:
                                    arg == "-" or not arg.startswith("-")
                                ).map1(lambda i:
                                    i.rstrip("\uFE0E\uFE0F")
                                ).filter1(lambda char: 
                                    ord(char) in table.cmap
                                )
                            ).map1(lambda char:
                                (char, char)
                            )
                        )).map1(lambda char:
                            C.delay(print, char, ucd.name(char, ""), "in", font_file_path)
                        )
                    ).reduce((lambda a, b: a.bind0(lambda: b)), C.delay())
                )
            ).reduce((lambda a, b: a.bind0(lambda: b)), C.delay())
        )
    ).reduce((lambda a, b: a.bind0(lambda: b)), C.delay()).make_task()
)



