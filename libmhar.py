#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-
# By HarJIT in 2021, ©.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""Har's monad library.

The other obvious abbreviation (harmlib) is apt for what extensive use thereöf does to stack
traces, but could be misconstrued as a call for violence."""

import abc, collections.abc, asyncio, glob, functools

def take_dispatcher(get_dispatcher, wrapped):
    def wrapper(*args, **kwargs):
        return get_dispatcher()(wrapped(*args, **kwargs))
    wrapper.__name__ = wrapped.__name__
    wrapper.__qualname__ = wrapped.__qualname__
    wrapper.__doc__ = wrapped.__doc__
    return wrapper

take_N = functools.partial(take_dispatcher, lambda: N)
take_G = functools.partial(take_dispatcher, lambda: G)
take_C = functools.partial(take_dispatcher, lambda: C)

class Monad(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def map1(self, a): # a takes 1 argument
        raise NotImplementedError
    @abc.abstractmethod
    def flatten(self):
        raise NotImplementedError
    # Recent Python versions don't have unbound method wrappers to type-check the self argument,
    #   so this can be called on the class passing an arbitrary object as self:
    @abc.abstractmethod
    def wrap(self_or_other):
        raise NotImplementedError
    #
    def map0(self, a): # a takes 0 arguments
        return self.map1(lambda i: a())
    def bind0(self, a): # >>; a takes 0 arguments
        return self.bind1(lambda i: a())
    def bind1(self, a): # >>=; a takes 1 argument
        return self.map1(a).flatten()

class IndexMonad(Monad):
    @abc.abstractmethod
    def map2(self, a): # a takes 2 arguments, second is index
        raise NotImplementedError
    #
    def map1(self, a): # a takes 1 argument
        return self.map2(lambda i, n: a(i))
    def bind1(self, a): # >>=; a takes 1 argument
        return self.bind2(lambda i, n: a(i))
    def bind2(self, a): # a takes 2 arguments, second is index
        return self.map2(a).flatten()

class EmptyableMonad(Monad):
    @staticmethod
    @abc.abstractmethod
    def empty():
        raise NotImplementedError
    #
    def filter0(self, a): # a takes 0 arguments; largely pointless, but for completeness
        return self.bind1(lambda i: type(self).wrap(i) if a() else type(self).empty())
    def filter1(self, a): # a takes 1 argument
        return self.bind1(lambda i: type(self).wrap(i) if a(i) else type(self).empty())
    def correlate(self, other, self_keyer, other_keyer, mappee):
        return self.bind1(lambda i:
            other.filter1(lambda j: self_keyer(i) == other_keyer(j))
                 .map1(lambda j: mappee(i, j)))

class EmptyableIndexMonad(EmptyableMonad, IndexMonad):
    def filter2(self, a): # a takes 2 arguments, second is index
        return self.bind2(lambda i, n: type(self).wrap(i) if a(i, n) else type(self).empty())

class ForcibleMonad(Monad):
    @abc.abstractmethod
    def force(self):
        raise NotImplementedError
    #
    def reduce(self, a, initial):
        accumulator = [initial]
        self.map1(lambda i: accumulator.append(a(accumulator.pop(), i))).force()
        return accumulator.pop()

class NullableMonad(ForcibleMonad, EmptyableIndexMonad):
    def __init__(self, value):
        self.value = value
    @take_N
    def map0(self, a):
        if self.value is not None:
            return a()
    @take_N
    def map1(self, a):
        if self.value is not None:
            return a(self.value)
    @take_N
    def map2(self, a):
        if self.value is not None:
            return a(self.value, 0)
    @take_N
    def flatten(self):
        return self.value.value
    @take_N
    def wrap(self_or_other):
        return self_or_other
    @staticmethod
    @take_N
    def empty():
        return None
    def force(self):
        return self

N = NullableMonad

class GeneratorMonad(EmptyableIndexMonad, ForcibleMonad, collections.abc.Iterable):
    def __init__(self, iterable):
        if not hasattr(iterable, "__iter__"):
            raise TypeError("creating generator monad of non-iterable object")
        self.iterable = iterable
    def __iter__(self):
        yield from self.iterable
    @take_G
    def force(self):
        self.iterable = tuple(self)
        return self
    @take_G
    def map0(self, a):
        yield from (a() for i in self)
    @take_G
    def map1(self, a):
        yield from (a(i) for i in self)
    @take_G
    def map2(self, a):
        yield from (a(i, n) for (n, i) in enumerate(self))
    @take_G
    def filter0(self, a):
        yield from (i for i in self if a())
    @take_G
    def filter1(self, a):
        yield from (i for i in self if a(i))
    @take_G
    def filter2(self, a):
        yield from (i for (n, i) in enumerate(self) if a(i, n))
    @take_G
    def bind0(self, a): # >>
        for i in self:
            yield from a()
    @take_G
    def bind1(self, a): # >>=
        for i in self:
            yield from a(i)
    @take_G
    def bind2(self, a):
        for n, i in enumerate(self):
            yield from a(i, n)
    @take_G
    def flatten(self):
        for i in self:
            yield from i
    @take_G
    def wrap(self_or_other):
        yield self_or_other
    @staticmethod
    @take_G
    def empty():
        yield from ()

G = GeneratorMonad

class PromiseMonad(IndexMonad, ForcibleMonad, collections.abc.Awaitable):
    def __new__(cls, coroutine, chains=()):
        if not chains and asyncio.iscoroutine(coroutine) and not issubclass(cls, CoroutineMonad):
            return CoroutineMonad(coroutine)
        ret = super().__new__(cls)
        ret.__init__(coroutine, chains)
        return ret
    def __init__(self, coroutine, chains=()):
        if not hasattr(coroutine, "__await__"):
            raise TypeError("creating promise monad of non-awaitable object")
        self.coroutine = coroutine
        self.resolved = False
        self.chains = chains
    def _await(self):
        chains = list(self.chains)
        coroutine = self.coroutine
        while 1:
            val = yield from coroutine.__await__()
            if not chains:
                return val
            coroutine = chains.pop(0)(val)
    def __await__(self):
        if not self.resolved:
            self._value, self.resolved = (yield from self._await()), True
        return self._value
    @take_C
    async def map1(self, a):
        return a(await self)
    def map2(self, a):
        return self.map1(lambda i: a(i, 0))
    def bind1(self, a): # >>=
        if not self.resolved:
            return PromiseMonad(self.coroutine, self.chains + (a,))
        return PromiseMonad(PromiseMonad.wrap(self._value).coroutine, self.chains + (a,))
    def bind2(self, a):
        return self.bind1(lambda i: a(i, 0))
    @take_C
    async def flatten(self):
        return await (await self)
    @take_C
    async def wrap(self_or_other):
        return self_or_other
    @take_C
    async def make_task(self): # get an actual asyncio coroutine, not merely an awaitable
        return await self
    def enqueue(self):
        task = self.make_task()
        asyncio.ensure_future(task)
        return task
    def force(self):
        a = self.__await__()
        try:
            while 1:
                next(a)
        except StopIteration as a:
            assert (self._value is None and not a.args) or (a.args[0] is self._value)
            return self
    @staticmethod
    @take_C
    async def delay(fun = (lambda: None), *args, **kwargs):
        await asyncio.sleep(0) # special-cased to yield to loop
        return fun(*args, **kwargs)

class CoroutineMonad(PromiseMonad, collections.abc.Coroutine):
    def send(self, value):
        self.coroutine.send(value)
    def throw(self, *args, **kws):
        self.coroutine.throw(*args, **kws)
    def close(self):
        self.coroutine.close()
    def make_task(self):
        return self

C = PromiseMonad



